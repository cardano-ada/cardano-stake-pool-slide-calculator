'use strict';

// Engineering Design Specification for Delegation and Incentives in Cardano–Shelley
// April 11, 2019

var maxAda = 45000000000;
var epoch = (1 / 73); // epoch 365/5 days
var circulatingAda = 0;
if (circulatingAda <= maxAda) {
  circulatingAda = inputs.totalSupplyAda;
} else {
  circulatingAda = maxAda;
}
var k = inputs.k; // desired number of pools k ∈ N+
var saturation = (circulatingAda / k);
var delegatedAdaPercent = 0;
if ((inputs.delegatedToPoolPercent / 100) <= 1) {
  delegatedAdaPercent = (inputs.delegatedToPoolPercent / 100);
} else {
  delegatedAdaPercent = 1;
}
var delegatedAda = (delegatedAdaPercent * saturation);
var pledgedAdaPercent = 0;
if ((inputs.pledgedToPoolPercent / 100) <= 1) {
  pledgedAdaPercent = (inputs.pledgedToPoolPercent / 100);
} else {
  pledgedAdaPercent = 1;
}
var pledgedAda = (pledgedAdaPercent * saturation);
var costAdaPerEpoch = (inputs.costPerEpoch / inputs.usdToAdaExchangeRate);
var treasuryTaxPercent = (inputs.treasuryTax / 100); // 5.4.4 Treasury
var tTax = 0; // Treasury [0, 1]
if (treasuryTaxPercent < 1) {
  tTax = treasuryTaxPercent;
} else {
  tTax = 1;
}

// Not in Specification
var pAda = ((inputs.pledgedToPoolPercent / 100) * saturation);
var dAda = ((inputs.delegatedToPoolPercent / 100) * saturation);
var poolCapacityRemaining = ((saturation) - (pAda + dAda));
var poolCapacityRemainingPercent = poolCapacityRemaining / saturation;
var poolRank = math.abs((inputs.k * (inputs.poolRank/100)) -1);
var probSat = math.max((dAda -(saturation * poolRank)), 0);
// Pledge to Delegation Ratio
var PDR = pledgedAda / (saturation - pledgedAda);

// 5.4.3 Monetary Expansion
var monetaryExpansionPercent = (inputs.incentivePercentYear / 100);
var p = 0; // expansion rate ρ ∈ [0, 1]
if (monetaryExpansionPercent < 1) {
  p = monetaryExpansionPercent;
} else {
  p = 1;
}
var monetaryExpansion = math.min((inputs.blockSuccessEpoch / 100) ,1) * p * (maxAda - inputs.totalSupplyAda);

var monetaryExpansioNoBlockSuccessEpoch = p * (maxAda - inputs.totalSupplyAda);
// Not in 5.4.3 Monetary Expansion Specification
var maxPoolPotential = ((p * (maxAda - inputs.totalSupplyAda)) / k);
var monetaryExpansionNoblockSuccessEpoch = p * (maxAda - inputs.totalSupplyAda);


// 5.5.2 Pool Rewards - optimal
var R = (monetaryExpansion * epoch); //total available rewards for the epoch (in ada)
var a0 = inputs.pledgeInfluence; // a0 ∈ [0, ∞) owner-stake influence on pool rewards
var z0 = (1 / inputs.k); // size of a saturated pool
var o = ((pledgedAda + delegatedAda) / circulatingAda); // relative stake
var oi = math.min(o, z0); // relative stake of the pool OR size of a saturated pool
var s = (pledgedAda / circulatingAda); // relative stake of the pool owner(s)
var si = math.min(s, z0); // relative stake of the pool owner(s) OR size of a saturated pool
var poolRewardsOptimal = ((R / (1 + a0)) * (oi + si * a0 * ((oi - (si * (z0 - oi / z0))) / z0)));
// Staying consistent
var pRmax = ((R / (1 + a0)) * (z0 + (z0 * a0 * (z0 - (z0 * (z0 - z0)) / z0)) / z0));
var pRmin = (R / (1 + a0)) / k;
var sybilImpact = (pRmin - pRmax);
var R2 = (monetaryExpansionNoblockSuccessEpoch * epoch);
var pR2max = ((R2 / (1 + a0)) * (z0 + (z0 * a0 * (z0 - (z0 * (z0 - z0)) / z0)) / z0));
var pR2min = (R2 / (1 + a0)) / k;
var sybilImpactR2 = (pR2min - pR2max);

// 5.5.3 Reward Splitting inside a pool - Myopic
var c = costAdaPerEpoch;
var m = 0;
if ((inputs.feePool / 100) <= 1) {
  m = (inputs.feePool / 100);
} else {
  m = 1;
}
var f = poolRewardsOptimal;
// Pool Operator Reward
var operatorR = 0;
if (f <= c) {
  operatorR = f;
} else {
  operatorR = c + (f - c) * (m + (1 - m) * (s / o));
}
// Pool Member Reward
var poolDelegated = (inputs.pledgedToPoolPercent / 100);
var memberR = 0;
if (f <= c) {
  memberR = 0;
} else {
  memberR = (f - c) * (1 - m) * (poolDelegated / o);
}

// 5.6.1 Pool Desirability and Ranking - Potential
// "min(s,z0)" math.min(pledgedAdaPercent, z0) Given (100% pledged = z0)
var saturatedPoolRewards = (R / (1 + a0)) * (z0 + math.min(s, z0) * a0);
var sR = (R / (1 + a0)) * (z0 + (pledgedAdaPercent * a0));
var desirability = 0;
if (saturatedPoolRewards <= c) {
  desirability = 0;
} else {
  desirability = (saturatedPoolRewards - c) * (1 - m);
}

var saturatedPoolRewardsNoA0 = (R2 / (1 + 0)) * (z0 + math.min(s, z0) * 0);
var d2 = (R2 / (1 + a0)) * (z0 + math.min(s, z0) * a0);;

// 5.6.2 Non-Myopic Pool Stake
// We predict that pools with rank ≤ k will eventually be saturated
// whereas pools with rank > k will lose all members and only consist of the owner(s)
var nonMyopicStake = 0;
if (poolRank <= k) {
  nonMyopicStake = math.max(o, z0);
} else {
  nonMyopicStake = pledgedAda;
}
// non-myopic stake σnm
var nMs = 0;
if ((delegatedAda + pledgedAda) <= saturation) {
  nMs = (delegatedAda + pledgedAda);
} else {
  nMs = saturation;
}

var totalDelegableAda = (circulatingAda * (inputs.delegableAda / 100));
var dA = ((inputs.delegableAda / 100) * inputs.totalSupplyAda);
var pCr = math.max((dA -(saturation * poolRank)), 0);
var poolFeeReward = (desirability * dAda * m);

return {
 delegation: [{
    'dAda': totalDelegableAda,
    'probSat': pCr,
    'dAdaS': math.ceil((totalDelegableAda / saturation))
  }],
 a: [{
    'Pool Saturation ADA': saturation,
    'Pool Capacity Remaining ADA': poolCapacityRemaining,
    'Pool Capacity Remaining %': poolCapacityRemainingPercent
  }],
  poolSpec: [{
    'pledgedToPoolPercent': pAda,
    'delegatedToPoolPercent': dAda,
    'poolRank': poolRank
  }],
  nonmyopiclevel: [{
    'totalPoolReward': saturatedPoolRewardsNoA0,
    'potentialAfterSybil': (saturatedPoolRewardsNoA0 + sybilImpactR2),
    'desirability': sybilImpactR2
  }],
  poolViewPledge: [{
    'potentialBeforePledge': (saturatedPoolRewardsNoA0 + sybilImpactR2),
    'potentialAfterPledge': d2,
    'pledgeInfluence': d2 - (saturatedPoolRewardsNoA0 + sybilImpactR2)
  }],
  poolViewCostMargin: [{
    'pledge': (1 - ((saturation - pAda) / saturation)) * d2,
    'cost': c,
    'margin': (((((saturation - pAda) / saturation) * d2) - c) * m)
  }],
  poolOperatorView: [{
    'desirability': d2 - (((((saturation - pAda) / saturation) * d2) - c) * m) - c,
    'operatorRewards': operatorR * (1 - tTax),
    'pledgeDelegationRatio': PDR,
  }]
};