# Cardano Stake Pool Slide Calculator

**First, the reason this calculator exists is to enable Cardano stake pool operators and or owners to think and see long term, non-myopic. It was never intended to be 100% accurate, it is correct to a degree, but that degree of correctness is not material here, it's not the reason this slide calculator exists.**

JSCalc.io is a free and open-source web service that reduces the task of creating an embeddable, mobile-friendly calculator to writing a single Javascript function.

**This Cardano stake pool slide calculator was made for ease of use via JSCalc.io**

The PDF folder contains [Engineering Design Specification for Delegation and Incentives in Cardano Shelley - aka - delegation_design_spec.pdf](/pdf/delegation_design_spec.pdf) which the js references.

**Direct link to working [Cardano Stake Pool Slide Calculator](https://jscalc.io/calc/hRreLLiiFQaMWc3A) on JSCalc.io**

